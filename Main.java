import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        char operator;
        Double number1, number2;

        Scanner input = new Scanner(System.in);

        System.out.println("Entrer un opérateur:  \n - add \n - sub \n - multiply \n - divide");
        operator = input.next().charAt(0);

        System.out.println("Entrer le premier chiffre");
        number1 = input.nextDouble();

        System.out.println("Entrer le second chiffre");
        number2 = input.nextDouble();

        switch (operator) {

            case 'a':
                System.out.println(add(number1, number2));
                break;

            case 's':
                System.out.println(sub(number1, number2));
                break;

            case 'm':
                System.out.println(multiply(number1, number2));
                break;

            case 'd':
                System.out.println(divide(number1, number2));
                break;

            default:
                System.out.println("Erreur");
                break;
        }

        input.close();

    }

    public static Double add(double a, double b) {
        return a + b;
    }

    public static Double sub(double a, double b) {
        return a - b;
    }

    public static Double multiply(double a, double b) {
        return a * b;
    }

    public static Double divide(double a, double b) {
        return a / b;
    }
}
